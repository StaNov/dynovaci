﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class WholeGameTest
{
    private PumpkinStories _stories;
    private LocationService _locationService;

    [UnityTest]
    public IEnumerator TestWholeGame()
    {
        AudioListener.volume = 0;
        SceneManager.LoadScene(0);
        yield return null;
        _stories = Object.FindObjectOfType<InitObject>().Stories;
        _locationService = Object.FindObjectOfType<LocationService>();
        Assert.IsNotNull(_locationService);

        yield return WaitWhile(() => GameObject.Find("IntroScreen/ContinueButton") == null);
        GameObject.Find("IntroScreen/ContinueButton").GetComponent<Button>().onClick.Invoke();

        yield return WaitWhile(() => GameObject.Find("AllowButton") == null);
        Assert.AreEqual(LocationServiceStatus.Stopped, _locationService.Status);
        GameObject.Find("AllowButton").GetComponent<Button>().onClick.Invoke();

        yield return null; yield return null;
        yield return WaitWhile(() => GameObject.Find("LocationAllowedScreen/Text") == null);
        GameObject.Find("LocationAllowedScreen/TurnOnButton").GetComponent<Button>().onClick.Invoke();

        yield return WaitWhile(() => GameObject.Find("TravelScreen/Text") == null);
        Assert.AreEqual(LocationServiceStatus.Running, _locationService.Status);
        GameObject.Find("TravelScreen/SetDebugCoordsButton").GetComponent<Button>().onClick.Invoke();
        
        int i = 0;
        yield return AssertItem("á", i++, "z");
        yield return AssertItem("é", i++, "s");
        yield return AssertItem("š", i++, "a");
        yield return AssertItem("BůBáK", i++, "j");
        
        yield return WaitWhile(() => GameObject.Find("EndScreen/FeedbackInput") == null);
        GameObject.Find("EndScreen/FeedbackInput").GetComponent<InputField>().text = "Test Feedback";
        GameObject.Find("EndScreen/SendFeedbackButton").GetComponent<Button>().onClick.Invoke();
        Assert.AreEqual("Test Feedback", GameObject.Find("FeedbackInput").GetComponent<Feedback>().DebugLastFeedback);
        Assert.IsTrue(GameObject.Find("EndScreen/SentConfirmation").activeInHierarchy);
        Assert.AreEqual(1, Resources.FindObjectsOfTypeAll<Notifier>()[0].DebugNotifiedCount);
    }

    private IEnumerator AssertItem(string puzzleSolution, int index, string personTag)
    {
        yield return WaitWhile(() => GameObject.Find("StoryScreen/ContinueButton") == null);
        yield return null;
        AudioSource storyAudioSource = GameObject.Find("StoryScreen").GetComponent<AudioSource>();
        Assert.AreEqual("story_" + personTag, storyAudioSource.clip.name);
        Assert.IsTrue(storyAudioSource.isPlaying);
        GameObject.Find("StoryScreen/ContinueButton").GetComponent<Button>().onClick.Invoke();

        AudioSource beeper = GameObject.Find("FindingScreen/Beeper").GetComponent<AudioSource>();
        Assert.IsTrue(beeper.clip.name.StartsWith("beep_" + personTag));
        Assert.IsTrue(beeper.isPlaying);

        yield return WaitWhile(() => GameObject.Find("FindingScreen/Debug/Near") == null);
        Button nearButton = GameObject.Find("FindingScreen/Debug/Near").GetComponent<Button>();

        GameObject findingScreen = GameObject.Find("FindingScreen");
        while (findingScreen.activeInHierarchy)
        {
            nearButton.onClick.Invoke();
            yield return null;
        }

        yield return WaitWhile(() => GameObject.Find("FoundScreen/Image") == null);
        Assert.IsTrue(GameObject.Find("FoundScreen/Image").GetComponent<Image>().sprite.name.StartsWith(index + 1 + "-"));
        Assert.AreEqual(_stories.Stories[index].FoundText, GameObject.Find("FoundScreen/Text").GetComponent<Text>().text);
        AudioSource foundAudioSource = GameObject.Find("FoundScreen").GetComponent<AudioSource>();
        Assert.AreEqual("found_" + personTag, foundAudioSource.clip.name);
        Assert.IsTrue(foundAudioSource.isPlaying);
        GameObject.Find("FoundScreen/ContinueButton").GetComponent<Button>().onClick.Invoke();

        yield return WaitWhile(() => GameObject.Find("PuzzleScreen/InputField") == null);
        AudioSource puzzleAudioSource = GameObject.Find("PuzzleScreen").GetComponent<AudioSource>();
        Assert.AreEqual("puzzle_" + personTag, puzzleAudioSource.clip.name);
        Assert.IsTrue(puzzleAudioSource.isPlaying);
        InputField puzzleInput = GameObject.Find("PuzzleScreen/InputField").GetComponent<InputField>();

        puzzleInput.text = puzzleSolution;
        yield return null;
        Assert.IsNotNull(GameObject.Find("PuzzleScreen/ValidationText"), "Puzzle solution not recognized");

        GameObject.Find("PuzzleScreen/ContinueButton").GetComponent<Button>().onClick.Invoke();
    }

    private delegate bool While();
    private static IEnumerator WaitWhile(While whileCondition)
    {
        float startTime = Time.time;
        
        while (whileCondition())
        {
            if (Time.time > startTime + 5)
            {
                throw new Exception("Timeout waiting");
            }
            
            yield return null;
        }
    }
}
