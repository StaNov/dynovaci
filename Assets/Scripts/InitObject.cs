﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InitObject : MonoBehaviour
{
    public static InitObject Instance => _instance;

    private static InitObject _instance;

    public static bool WasGameInitialized => _instance != null;

    public PumpkinStories Stories;

    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
        
        UnityEngine.Screen.sleepTimeout = SleepTimeout.NeverSleep;
        
#if UNITY_EDITOR
        LevelHolder.ResetLevel();
#endif

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
