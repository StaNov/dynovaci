using UnityEngine;

#if !UNITY_EDITOR
using UnityEngine.Networking;
#endif

public class Notifier : MonoBehaviour
{
    public int DebugNotifiedCount { get; private set; }
    
    private void OnDisable()
    {
#if !UNITY_EDITOR
        UnityWebRequest.Get("http://stanov.cz/dynovaci.php").SendWebRequest();
#else
        Debug.Log("Notified!");
        DebugNotifiedCount++;
#endif
    }
}