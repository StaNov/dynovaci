using UnityEngine;

public static class LevelHolder
{
    public static int Level
    {
        get => PlayerPrefs.GetInt("level", 0);

        set
        {
            PlayerPrefs.SetInt("level", value);
            PlayerPrefs.Save();
        }
    }
    
    public static void ResetLevel()
    {
        Level = 0;
    }
}