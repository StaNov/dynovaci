using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Stories", menuName = "Pumpkin Stories", order = 0)]
public class PumpkinStories : ScriptableObject
{
    public Story[] Stories;

    public Story Current => Stories[LevelHolder.Level];
}

[Serializable]
public class Story
{
    public AudioClip StoryAudio;
    public string StoryName;
    [TextArea]
    public string StoryText;
    public float Latitude = 49.217594f;
    public float Longitude = 16.584143f;
    [TextArea]
    public string PuzzleText = "Puzzle input";
    public string[] PuzzleSolutionsLowerCase = {"Solution"};
    public AudioClip PuzzleAudio;
    public Sprite FoundImage;
    [TextArea]
    public string FoundText;
    public AudioClip FoundAudio;
    public Beeps Beeps;
}

[Serializable]
public class Beeps
{
    public AudioClip[] Silent;
    public AudioClip[] Medium;
    public AudioClip[] Loud;
}