﻿using System.Collections;
using UnityEngine;

#if !UNITY_EDITOR
using UnityEngine.Android;
#endif

public class LocationService : MonoBehaviour
{
    public PumpkinStories PumpkinStories;

    private bool _debugIsStarted;
    private bool _debugIsLocationTurnedOn;
    private bool _debugIsInRange;
    
    public LocationServiceStatus Status
    {
        get
        {
#if UNITY_EDITOR
            if (!_debugIsStarted)
                return LocationServiceStatus.Stopped;

            if (!_debugIsLocationTurnedOn)
                return LocationServiceStatus.Failed;
            
            return LocationServiceStatus.Running;
#else
            return Input.location.status;
#endif
        }
    }

    public float Latitude
    {
        get
        {
#if UNITY_EDITOR
            return _debugIsInRange ? 49.218f : 0;
#else
            return Input.location.lastData.latitude;
#endif
        }
    }

    public float Longitude
    {
        get
        {
#if UNITY_EDITOR
            return _debugIsInRange ? 16.584321f : 0;
#else
            return Input.location.lastData.longitude;
#endif
        }
    }

    public int Accuracy
    {
        get
        {
#if UNITY_EDITOR
            return Mathf.FloorToInt(Time.time) % 10;
#else
            return Mathf.FloorToInt(Input.location.lastData.horizontalAccuracy);
#endif
        }
    }

    public int DistanceToCurrentTarget()
    {
        return DistanceTo(PumpkinStories.Current.Latitude, PumpkinStories.Current.Longitude);
    }
    
    private int DistanceTo(float latitude, float longitude)
    {
        float lat1 = Latitude;
        float lat2 = latitude;
        float lon1 = Longitude;
        float lon2 = longitude;
        var R = 6378.137f; // Radius of earth in KM
        var dLat = lat2 * Mathf.PI / 180 - lat1 * Mathf.PI / 180;
        var dLon = lon2 * Mathf.PI / 180 - lon1 * Mathf.PI / 180;
        var a = Mathf.Sin(dLat/2) * Mathf.Sin(dLat/2) +
                Mathf.Cos(lat1 * Mathf.PI / 180) * Mathf.Cos(lat2 * Mathf.PI / 180) *
                Mathf.Sin(dLon/2) * Mathf.Sin(dLon/2);
        var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1-a));
        var d = R * c;
        float computedDistance = d * 1000;
        return Mathf.FloorToInt(computedDistance * _debugDistanceModifier);
    }
    
    private float _debugDistanceModifier = 1;

    public void MoveCloser()
    {
        _debugDistanceModifier -= 0.1f;
        _debugDistanceModifier = Mathf.Max(_debugDistanceModifier, 0.000001f);
    }

    public void MoveFurther()
    {
        _debugDistanceModifier += 0.1f;
    }

    public void ResetDebugDistanceModifier()
    {
        _debugDistanceModifier = 1;
    }

    public IEnumerator StartLocationService()
    {
        while (Status == LocationServiceStatus.Stopped)
        {
#if UNITY_EDITOR
            _debugIsStarted = true;
#else
            Input.location.Start(0.1f, 0.1f);
#endif
            yield return null;
        }
    }

#if UNITY_EDITOR
    private bool _debugPermissionGranted;
#endif

    public bool IsLocationPermissionGranted()
    {
#if UNITY_EDITOR
        return _debugPermissionGranted;
#else
        return Permission.HasUserAuthorizedPermission(Permission.FineLocation);
#endif
    }

    public void RequestLocationPermission()
    {
#if UNITY_EDITOR
        _debugPermissionGranted = true;
#else
        Permission.RequestUserPermission(Permission.FineLocation);
#endif
    }

    public void DebugEnableEverything()
    {
        StartCoroutine(StartLocationService());
        RequestLocationPermission();
        DebugTurnOnLocation();
        SetDebugCoordinatesToTheCloseOnes();
    }

    public void SetDebugCoordinatesToTheCloseOnes()
    {
        _debugIsInRange = true;
    }

    public void DebugTurnOnLocation()
    {
        _debugIsLocationTurnedOn = true;
    }
}
