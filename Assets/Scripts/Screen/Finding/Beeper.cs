﻿using UnityEngine;
using Random = UnityEngine.Random;

public class Beeper : MonoBehaviour
{
    public AudioSource BeeperSource;
    public PumpkinStories Stories;
    public LocationService LocationService;

    private int _currentBeepIndex;
    private AudioClip _defaultBeep;

    public void Beep()
    {
        if (_defaultBeep is null)
        {
            _defaultBeep = BeeperSource.clip;
        }
        
        BeeperSource.Stop();

        AudioClip[] currentBeeps;
        if (LocationService.DistanceToCurrentTarget() > 20)
        {
            currentBeeps = Stories.Current.Beeps.Silent;
            BeeperSource.volume = 0.4f;
        }
        else if (LocationService.DistanceToCurrentTarget() > 10)
        {
            currentBeeps = Stories.Current.Beeps.Medium;
            BeeperSource.volume = 0.7f;
        }
        else
        {
            currentBeeps = Stories.Current.Beeps.Loud;
            BeeperSource.volume = 1f;
        }
        
        BeeperSource.clip = currentBeeps.Length > 0 
            ? currentBeeps[UpdatedCurrentBeepIndex(currentBeeps.Length)] 
            : _defaultBeep;
        
        BeeperSource.Play();
    }

    private int UpdatedCurrentBeepIndex(int currentBeepsLength)
    {
        _currentBeepIndex += Random.Range(1, currentBeepsLength);
        _currentBeepIndex %= currentBeepsLength;
        return _currentBeepIndex;
    }
}
