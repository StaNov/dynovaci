﻿using UnityEngine;
using UnityEngine.UI;

public class DistanceText : MonoBehaviour
{
    public Text text;
    public LocationService LocationService;

    void Update()
    {
        text.text = LocationService.DistanceToCurrentTarget() + " m";
    }
}
