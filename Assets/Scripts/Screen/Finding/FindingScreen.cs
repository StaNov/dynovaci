﻿using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class FindingScreen : MonoBehaviour
{
    public ParticleSystem ParticleSystem;
    public LocationService LocationService;
    public ScreensManager ScreensManager;
    public Beeper Beeper;

    private void OnEnable()
    {
#if UNITY_EDITOR
        if (LocationService.Status != LocationServiceStatus.Running)
        {
            LocationService.DebugEnableEverything();
        }
#endif

        StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        while (LocationService.DistanceToCurrentTarget() > 1)
        {
            Beeper.Beep();
            ParticleSystem.Play();
            
            float lastBeep = Time.time;
            while (Time.time - lastBeep < WaitTime())
                yield return null;
        }

        LocationService.ResetDebugDistanceModifier();
        ScreensManager.ShowScreen(Screen.FoundScreen);
    }

    private float WaitTime()
    {
        int distance = LocationService.DistanceToCurrentTarget();
        return Mathf.Min(0.05f * distance, 10f);
    }
}
