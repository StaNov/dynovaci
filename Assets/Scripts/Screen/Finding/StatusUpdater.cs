using UnityEngine;
using UnityEngine.UI;

public class StatusUpdater : MonoBehaviour
{
    public LocationService LocationService;
    public Text status;

    private void Update()
    {
        status.text = GetStatus();

        var color = status.color;
        color.a = status.text.StartsWith("OK") ? 0.3f : 1f;
        status.color = color;
    }

    private string GetStatus()
    {
        if (!LocationService.IsLocationPermissionGranted())
            return "Nepovoleno zjišťování polohy.";

        if (LocationService.Status != LocationServiceStatus.Running)
            return "Zjištění polohy je nedostupné. Máš jej zapnuté?";

        int accuracy = Mathf.FloorToInt(LocationService.Accuracy);
        if (LocationService.Accuracy > 5)
            return "Slabý signál GPS, odchylka je " + accuracy + " metrů.";

        return "OK, přesnost GPS je " + accuracy + "m.";
    }
}