﻿using UnityEngine;

public class TravelScreen : MonoBehaviour
{
    public LocationService LocationService;
    public ScreensManager ScreensManager;

    void Update()
    {
        if (LocationService.DistanceToCurrentTarget() < 1000)
        {
            ScreensManager.ShowNextScreen();
        }
    }
}
