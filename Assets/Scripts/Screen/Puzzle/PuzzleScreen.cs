﻿using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleScreen : MonoBehaviour
{
    public PumpkinStories Stories;
    public Text PuzzleText;
    public InputField InputField;
    public ScreensManager ScreensManager;
    public GameObject[] ToEnableOnCorrectAnswer;
    public AudioSource AudioSource;

    private void OnEnable()
    {
        PuzzleText.text = Stories.Current.PuzzleText;
        InputField.text = "";
        foreach (var enableObject in ToEnableOnCorrectAnswer)
            enableObject.SetActive(false);
        AudioSource.clip = Stories.Current.PuzzleAudio;
        AudioSource.Play();
    }

    void Update()
    {
        string normalized = RemoveDiacritics(InputField.text).ToLower();
        if (Stories.Current.PuzzleSolutionsLowerCase.Contains(normalized))
            foreach (var enableObject in ToEnableOnCorrectAnswer)
                enableObject.SetActive(true);
    }

    public void OnContinueButton()
    {
        if (LevelHolder.Level < Stories.Stories.Length - 1)
        {
            LevelHolder.Level += 1;
            ScreensManager.ShowScreen(Screen.StoryScreen);
        }
        else
        {
            ScreensManager.ShowScreen(Screen.EndScreen);
        }
    }
    
    private static string RemoveDiacritics(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            return text;

        text = text.Normalize(NormalizationForm.FormD);
        var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
        return new string(chars).Normalize(NormalizationForm.FormC);
    }
}
