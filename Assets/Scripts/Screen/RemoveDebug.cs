﻿using UnityEngine;

public class RemoveDebug : MonoBehaviour
{
    void Awake()
    {
#if !DEBUG
        Destroy(gameObject);
#endif
    }
}
