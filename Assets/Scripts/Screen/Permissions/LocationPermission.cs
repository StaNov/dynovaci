﻿using System.Collections;
using UnityEngine;

public class LocationPermission : MonoBehaviour
{

    public ScreensManager ScreensManager;
    public LocationService LocationService;

    private void Update()
    {
        if (LocationService.IsLocationPermissionGranted())
        {
            ScreensManager.ShowNextScreen();
        }
    }

    public void ShowLocationPermissionDialog()
    {
        LocationService.RequestLocationPermission();
    }
}
