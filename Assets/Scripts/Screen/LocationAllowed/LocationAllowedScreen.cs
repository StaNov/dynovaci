﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationAllowedScreen : MonoBehaviour
{
    public LocationService LocationService;
    public ScreensManager ScreensManager;
    
    IEnumerator Start()
    {
        yield return LocationService.StartLocationService();
    }

    void Update()
    {
        if (LocationService.Status == LocationServiceStatus.Running)
        {
            ScreensManager.ShowNextScreen();
        }
    }
}
