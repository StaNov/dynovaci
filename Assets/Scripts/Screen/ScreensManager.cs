﻿using System;
using UnityEngine;

public class ScreensManager : MonoBehaviour
{
    private Screen _current;
    void Awake()
    {
        if (InitObject.WasGameInitialized)
        {
            ShowScreen(Screen.IntroScreen);
        }
        else
        {
            LevelHolder.ResetLevel();
            SetCurrentToActiveScreen();
        }
    }

    private void SetCurrentToActiveScreen()
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject.activeInHierarchy)
            {
                _current = (Screen) Enum.Parse(typeof(Screen), child.name);
                return;
            }
        }
    }

    public void ShowNextScreen()
    {
        Screen[] values = (Screen[]) Enum.GetValues(typeof(Screen));
        int currentIndex = Array.IndexOf(values, _current);
        ShowScreen(values[currentIndex + 1]);
    }

    public void ShowScreen(Screen screen)
    {
        _current = screen;
        
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(child.name == screen.ToString());
        }
    }
}

public enum Screen
{
    IntroScreen = 0,
    PermissionsScreen = 1,
    LocationAllowedScreen = 2,
    TravelScreen = 3,
    StoryScreen = 4,
    FindingScreen = 5,
    FoundScreen = 6,
    PuzzleScreen = 7,
    EndScreen = 8
}