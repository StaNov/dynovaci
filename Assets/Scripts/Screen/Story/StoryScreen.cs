﻿using UnityEngine;
using UnityEngine.UI;

public class StoryScreen : MonoBehaviour
{
    public AudioSource AudioSource;
    public Text HeaderText;
    public Text StoryText;
    public PumpkinStories Stories;

    private void Awake()
    { 
#if UNITY_EDITOR
        LevelHolder.ResetLevel();
#endif
    }

    void OnEnable()
    {
        StoryText.text = Stories.Current.StoryText;
        HeaderText.text = Stories.Current.StoryName;

        AudioSource.clip = Stories.Current.StoryAudio;
        AudioSource.Stop();
        AudioSource.Play();
    }
}
