﻿using UnityEngine;
using UnityEngine.UI;

public class FoundScreen : MonoBehaviour
{
    public Image FoundImage;
    public Text FoundText;
    public PumpkinStories Stories;
    public AudioSource AudioSource;
    
    private void OnEnable()
    {
        FoundText.text = Stories.Current.FoundText;
        FoundImage.sprite = Stories.Current.FoundImage;
        AudioSource.clip = Stories.Current.FoundAudio;
        AudioSource.Play();
    }
}
