﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayAgainButton : MonoBehaviour
{
    public void OnClick()
    {
        LevelHolder.ResetLevel();
        SceneManager.LoadScene(0);
    }
}
