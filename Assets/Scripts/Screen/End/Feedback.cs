﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Feedback : MonoBehaviour
{
    public InputField FeedbackInput;
    public Text SentError;
    public Text SentConfirmation;

    public string DebugLastFeedback { get; private set; }

    void OnEnable()
    {
        FeedbackInput.text = "";
        SentError.gameObject.SetActive(false);
        SentConfirmation.gameObject.SetActive(false);
    }

    public void OnSendButtonClick()
    {
#if !UNITY_EDITOR
        StartCoroutine(OnSendButtonClickCoroutine());
#else
        DebugLastFeedback = FeedbackInput.text;
        Debug.Log("Feedback sent: " + DebugLastFeedback);
        SentConfirmation.gameObject.SetActive(true);
#endif
    }

    private IEnumerator OnSendButtonClickCoroutine()
    {
        var form = new WWWForm();
        form.AddField("feedback", FeedbackInput.text);
        var request = UnityWebRequest.Post("http://stanov.cz/dynovaci.php", form);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            SentError.gameObject.SetActive(true);
        }
        else
        {
            SentConfirmation.gameObject.SetActive(true);
        }
    }
}
