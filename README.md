# Dýňováci

A little game with a small walk around our neighborhood.

[![Get it on Google Play](https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=cz.stanov.dynaci)

![](GooglePlay/logo-play.png)
![](GooglePlay/screen-phone-1.png)
![](GooglePlay/screen-phone-2.png)
![](GooglePlay/screen-phone-3.png)